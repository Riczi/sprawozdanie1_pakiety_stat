CarsData <- read.csv("other/data.csv", header = TRUE);

years=c(2000, 2002, 2004, 2006)
years=2002:2009
par(mfrow=c(2, 8))
for (year in years){
    boxplot(CarsData$co2[CarsData$fuel_type == "Diesel" & CarsData$year == year])
}
for (year in years){
    boxplot(CarsData$co2[CarsData$fuel_type == "Petrol" & CarsData$year == year])
}
