library(ggplot2)
DC <- read.csv("other/data.csv",header = TRUE);

# DCf=DataCars$file[6000:16000]
# DCthc=DataCars$thc_emissions[6000:16000]

# DCf=DCf[!is.na(DCthc)]
# DCthc=DCthc[!is.na(DCthc)]
# table(DCf[DCthc==100])

# table(DataCars$thc_emissions[DataCars$file=='Part_A_Euro_IV_latest.csv'])
# table(DataCars$thc_emissions[DataCars$file=='Part_A_Euro_IV_may2003.csv'])
# table(DataCars$thc_emissions[DataCars$file=='Part_B_Euro_III_latest.csv'])
# table(DataCars$thc_emissions[DataCars$file=='Part_B_Euro_III_may2003.csv'])

# noise_level 
# co2 
# thc_emissions 
# co_emissions 
# nox_emissions 
# thc_nox_emissions
# particulates_emissions

table(DC$co2)
hist(DC$co2)
table(DC$co_emissions)
hist(DC$co_emissions)
hist(DC$co_emissions[DC$co_emissions<100])

table(DC$fuel_type[is.na(DC$co_emissions)])



table(DC$thc_emissions)
hist(DC$thc_emissions)
hist(DC$thc_emissions[DC$thc_emissions<1000])
sum(is.na(DC$thc_emissions))

table(DC$nox_emissions)
hist(DC$nox_emissions)
hist(DC$nox_emissions[DC$nox_emissions<1000])
sum(is.na(DC$nox_emissions))


nox_thc=DC$nox_emissions+DC$thc_emissions
table(nox_thc)
hist(nox_thc)
S<-hist(nox_thc[nox_thc<350])
sum(is.na(nox_thc))

sum(is.na(DC$nox_emissions) | is.na(DC$thc_emissions))



table(DC$thc_nox_emissions)
T<-hist(DC$thc_nox_emissions)

par(mfrow=c(2,2))
hist(nox_thc[nox_thc<350])
hist(DC$thc_nox_emissions[!is.na(DC$nox_emissions) & !is.na(DC$thc_emissions)])
hist(DC$nox_emissions[DC$nox_emissions<350 & !is.na(DC$nox_emissions) & !is.na(DC$thc_emissions)])
hist(DC$thc_emissions[DC$thc_emissions<350 & !is.na(DC$nox_emissions) & !is.na(DC$thc_emissions)])

grid.arrange(S, T, ncol = 1, nrow = 2)


DC$year[DC$co_emissions==-200]
DC$file[DC$co_emissions==-200]

IT=!is.na(DC$thc_nox_emissions) & !is.na(DC$nox_emissions) & !is.na(DC$thc_emissions)

DC$nox_emissions[IT]+DC$thc_emissions[IT]-DC$thc_nox_emissions[IT]

IT=!is.na(CarsData$thc_nox_emissions) & !is.na(CarsData$nox_emissions) & !is.na(CarsData$thc_emissions)

CarsData$nox_emissions[IT]+CarsData$thc_emissions[IT]-CarsData$thc_nox_emissions[IT]


