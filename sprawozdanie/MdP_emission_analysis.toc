\contentsline {section}{\numberline {1}Opis danych i cel analizy}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Skąd pochodzą dane?}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Cel analizy}{2}{subsection.1.2}%
\contentsline {section}{\numberline {2}Obsługa braków danych}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Braki danych dotyczące spalania}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Braki danych dotyczących emisji}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Emisja THC}{3}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Sprawdzenie poprawności imputacji}{5}{subsubsection.2.3.1}%
\contentsline {subsection}{\numberline {2.4}Emisja NOX}{5}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Sprawdzenie poprawności imputacji}{7}{subsubsection.2.4.1}%
\contentsline {section}{\numberline {3}Wizualna analiza danych}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Proporcje emisji wśród nowych samochodów}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Udział poszczególnych rodzajów paliw}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Zmiana znormalizowanej emisji w latach}{11}{subsection.3.3}%
\contentsline {section}{\numberline {4}Wnioski}{11}{section.4}%
