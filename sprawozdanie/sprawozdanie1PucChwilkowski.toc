\contentsline {section}{\numberline {1}Opis danych}{2}{section.1}%
\contentsline {section}{\numberline {2}Cel analizy}{6}{section.2}%
\contentsline {section}{\numberline {3}Obsługa braków danych}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Braki danych dotyczące spalania}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Braki danych dotyczących emisji}{7}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Emisja dźwięku}{7}{subsubsection.3.2.1}%
\contentsline {subsection}{\numberline {3.3}Emisja THC}{8}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Sprawdzenie poprawności imputacji}{11}{subsubsection.3.3.1}%
\contentsline {subsection}{\numberline {3.4}Emisja NOX}{13}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Sprawdzenie poprawności imputacji}{14}{subsubsection.3.4.1}%
\contentsline {subsection}{\numberline {3.5}Emisja THC\nobreakspace {}i\nobreakspace {}NOX}{15}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Emisja cząstek stałych (PM)}{16}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Progi podatkowe}{16}{subsection.3.7}%
\contentsline {section}{\numberline {4}Wizualna analiza danych}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}Proporcje emisji wśród nowych samochodów}{17}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Udział poszczególnych rodzajów paliw}{17}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Zmiana znormalizowanej emisji w latach}{20}{subsection.4.3}%
\contentsline {section}{\numberline {5}Wnioski}{20}{section.5}%
